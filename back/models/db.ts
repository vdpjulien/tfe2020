import mysql from 'mysql'

const db = mysql.createConnection({
    host : 'localhost',
    user : 'root',
    password : 'root',
    database : 'tfe2020'
});

db.connect((err) => {
    if(err){
        throw err;
    }
    console.log('Mysql Connected');
})

export default db;