import db from './db'

export class Rdv {

    private id : number;
    private dateDebut : Date;
    private dateFin : Date;
    private lieu : string;
    private statut : string;


    constructor(id : number, dateDebut : Date, dateFin : Date, 
        lieu : string, statut : string){

        this.id = id;
        this.dateDebut = dateDebut;
        this.dateFin = dateFin;
        this.lieu = lieu;
        this.statut = statut;

    }



    public static async getRdvs(){
        try {
            let sql = 'SELECT * from rdvs';
            db.query(sql, (err,res) => {
                if(err){
                    console.log('error:', err);
                }
                console.log("rdvs", res)
            })
        } catch (error) {
            console.log(error);
        }
    }


}
