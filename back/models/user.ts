import db from './db'

export class User {

    private id : number;
    private nom : string;
    private prenom : string;
    private dateNaissance : Date;
    private mail : string;
    private numeroTel : string;
    private password : string;
    private adresse : string;
    private numero : string;
    private ville : string;
    private codePostal : number;

    constructor(id:number, nom : string, prenom : string, 
        dateNaissance : Date, mail : string, numeroTel : string,
        password : string, adresse : string, numero : string,
        ville : string, codePostal : number){

        this.id = id;
        this.nom = nom;
        this.prenom = prenom;
        this.dateNaissance = dateNaissance;
        this.mail = mail;
        this.numeroTel = numeroTel;
        this.password = password;
        this.adresse = adresse;
        this.numero = numero;
        this.ville = ville;
        this.codePostal = codePostal;
    }



    public static async getUsers(){
        try {
            let sql = 'SELECT * from users';
            db.query(sql, (err,res) => {
                if(err){
                    console.log('error:', err);
                }
                console.log("users", res)
            })
        } catch (error) {
            console.log(error);
        }
    }


}
