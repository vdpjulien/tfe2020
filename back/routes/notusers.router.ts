import { Router, Request, Response, NextFunction} from 'express';
import { User } from '../models/user';
import { Rdv } from '../models/rdv';


export class NotUsersRouter {
    public router: Router;

    constructor(){
        this.router = Router();
        this.router.get('/users', User.getUsers);
        this.router.get('/rdvs', Rdv.getRdvs);
    }

}