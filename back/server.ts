import express from 'express';


import { NotUsersRouter } from './routes/notusers.router'

export class Server {

    private express: express.Application;

    constructor() {
        this.express = express();
        this.middleware();
        this.routes();
    }
    

    private middleware(): void {

        this.express.use(express.json());
        this.express.use(express.urlencoded({extended: false}));

    }

    private routes(): void {
        this.express.use('/api/notusers', new NotUsersRouter().router);
    }

    public start(): void {
        

        this.express.get('/', (req,res) => {
            res.send('Hello');
        });

        /*app.get('/createTest', (req, res) => {
            let sql = 'CREATE TABLE test(id int, title VARCHAR(255), PRIMARY KEY(id) )'
            db.query(sql, (err, result) => {

                if(err) throw err;
                console.log(result);
                res.send('Posts table created...');
            })
        })*/

        this.express.listen(5000, () => console.log('Server running'));
    }

}