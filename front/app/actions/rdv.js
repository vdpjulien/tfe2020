import axios from 'axios';
import { createMessage } from './messages';

import { GET_RDVS, DELETE_RDV, ADD_RDV } from './types';

// Get all RDVs 
export const getRDVs = () => dispatch => {
    axios.get('/api/rdvs/')
    .then(res => {
        dispatch({
            type: GET_RDVS,
            payload: res.data
        });
    })
    .catch(err => console.log(err));
}

// GET RANGE RDVS
// POST + START/END

// Delete RDV

export const deleteRDV = (id) => dispatch => {
    axios.delete(`/api/rdvs/${id}/`)
    .then(res => {
        dispatch(createMessage({ deleteRDV : "RDV Deleted" }));
        dispatch({
            type: DELETE_RDV,
            payload: id
        });
    })
    .catch(err => console.log(err));
}

//ADD RDV  
export const addRDV = (RDV) => dispatch => {
    axios.post('/api/rdvs/', RDV)
    .then(res => {
        dispatch(createMessage({ addRDV : "RDV Added" }));
        dispatch({
            type: ADD_RDV,
            payload: res.data
        });
    })
    .catch(err => console.log(err));
}