export const GET_USERS = "GET_USERS";
export const DELETE_USER = "DELETE_USER";
export const ADD_USER = "ADD_USER";
export const CREATE_MESSAGE = "CREATE_MESSAGE";
export const GET_RDVS = "GET_RDVS";
export const DELETE_RDV = "DELETE_RDV";
export const ADD_RDV = "ADD_RDV";