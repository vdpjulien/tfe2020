import axios from 'axios';
import { createMessage } from './messages';

import { GET_USERS, DELETE_USER, ADD_USER } from './types';

// Get Users 
export const getUsers = () => dispatch => {
    axios.get('/api/users/')
    .then(res => {
        dispatch({
            type: GET_USERS,
            payload: res.data
        });
    })
    .catch(err => console.log(err));
}

// Delete User

export const deleteUser = (id) => dispatch => {
    axios.delete(`/api/users/${id}/`)
    .then(res => {
        dispatch(createMessage({ deleteUser : "User Deleted" }));
        dispatch({
            type: DELETE_USER,
            payload: id
        });
    })
    .catch(err => console.log(err));
}

//ADD User  
export const addUser = (user) => dispatch => {
    axios.post('/api/users/', user)
    .then(res => {
        dispatch(createMessage({ addUser : "User Added" }));
        dispatch({
            type: ADD_USER,
            payload: res.data
        });
    })
    .catch(err => console.log(err));
}