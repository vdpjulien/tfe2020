import React, { Component, Fragment } from 'react';
import ReactDOM from 'react-dom';
import { HashRouter as Router, Route, Switch, Redirect} from "react-router-dom";

import { Provider as AlertProvider } from 'react-alert';
import AlertTemplate from 'react-alert-template-basic';

import Header from './layout/Header';
import ListeUsers from './users/ListeUsers';
import ListeRdv from './rdv/ListeRDV';
import Calendar from './calendar/calendar'
import Alerts from './layout/Alerts';

import { Provider } from 'react-redux';
import store from '../store'


//Alert options
const alertOptions = {
    timeout : 3000,
    position : 'bottom center'
}

class App extends Component {
    render() {
        return (
            <Provider store={store}>
                <AlertProvider template={AlertTemplate}{...alertOptions}>
                    <Router>
                        <Fragment>
                            <Header/>

                            <Switch>
                                <Route exact path="/" component={ListeUsers}/>
                                <Route exact path="/rdv" component={ListeRdv}/>
                                <Route exact path="/calendar" component={Calendar}/>
                            </Switch>

                            <Alerts/>
                        </Fragment>
                    </Router>
                </AlertProvider>
            </Provider>
        )
    }
}

ReactDOM.render(<App />, document.getElementById('app'));