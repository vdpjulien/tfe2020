import FullCalendar from '@fullcalendar/react';
import dayGridPlugin from '@fullcalendar/daygrid';
import timeGridPlugin from '@fullcalendar/timegrid';
import interactionPlugin from '@fullcalendar/interaction';

import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'

import './main.css'

export class calendar extends Component {
  static propTypes = {
    events: PropTypes.array.isRequired,
  }

  render() {
    return (
      <div>
        <FullCalendar
          plugins={[dayGridPlugin, timeGridPlugin, interactionPlugin]}
          headerToolbar={{
            left: 'prev next today',
            center: 'title',
            right: 'dayGridMonth,timeGridWeek,timeGridDay'
          }}
          initialView='dayGridMonth'
          editable={false}
          eventClick={this.handleEventClick}
          datesSet={this.handleDates}

        />
      </div>
    )
  }

  handleEventClick = (clickInfo) =>{}
  handleDates = (rangeInfo) =>{}
}

const mapStateToProps = (state) => ({

  events: state.calendarReducer.events
  
})

const mapDispatchToProps = {
  
}

export default connect(mapStateToProps, mapDispatchToProps)(calendar)
