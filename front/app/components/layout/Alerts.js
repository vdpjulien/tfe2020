import React, { Component, Fragment } from 'react';
import { withAlert } from 'react-alert';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';


export class Alerts extends Component {

    static propTypes = {
        message : PropTypes.object.isRequired
    }

    componentDidUpdate(prevProps){
        const { message, alert } = this.props

        if (message !== prevProps.message){
            if(message.deleteUser) alert.success(message.deleteUser);
            if(message.addUser) alert.success(message.addUser);
        }
    }
    
    render() {
        return <Fragment/>
    }
}

const mapStateToProps = state => ({
    message: state.messageReducer
})

export default connect(mapStateToProps)(withAlert()(Alerts));
