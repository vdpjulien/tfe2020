import React, { Component } from 'react'
import { Link } from "react-router-dom"

export class Header extends Component {
    render() {
        return (
            <div>
                <a href="/" style={{marginRight: '10px'}}>Eric Vandeputte</a>
                <Link style={{marginRight: '10px'}}  to ="/rdv"> Rdv </Link>
                <Link to ="/calendar"> Calendrier </Link>

            </div>
        )
    }
}

export default Header
