import React, { Component } from 'react'
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { addRDV } from '../../actions/rdv'


export class Form extends Component {

    state = {
        id : "",
        date : "",
        lieu : "",
    }

    static propTypes = {
        addRDV: PropTypes.func.isRequired
    }

    onChange = event => this.setState({[event.target.name] : event.target.value});
    onSubmit = event => {
        event.preventDefault();
        const { id, date, lieu } = this.state;
        const statut = "En attente";
        const rdv  = { id, date, lieu, statut };
        this.props.addRDV(rdv);
        this.setState({
            id : "",
            date : "",
            lieu : "",
        })
    }
    render() {
        const { id, date, lieu } = this.state;
        return (
            <div>
                <h1>Add Rdv Form</h1>
                <form onSubmit={this.onSubmit}>
                    <div>
                        <label>Id</label>
                        <input
                            type="number"
                            name="id"
                            onChange={this.onChange}
                            value={id}
                        />
                    </div>
                    <div>
                        <label>Date du Rdv</label>
                        <input
                            type="datetime-local"
                            name="date"
                            onChange={this.onChange}
                            value={date}
                        />
                    </div>
                    <div>
                        <label>Lieu du rdv</label>
                        <input
                            type="text"
                            name="lieu"
                            onChange={this.onChange}
                            value={lieu}
                        />
                    </div>
                    <div>
                        <button type="submit">
                            Envoyer
                        </button>
                    </div>
                </form>
            </div>
        )
    }
}

export default connect(null, { addRDV })(Form);
