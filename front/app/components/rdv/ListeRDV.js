import React, { Fragment } from 'react';
import Form from './Form';
import Rdv from './Rdv'

export default function ListeRdv() {
    return (
        <Fragment>
            <Form/>
            <Rdv/>
        </Fragment>
    )
}
