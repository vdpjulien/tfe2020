import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { getRDVs, deleteRDV } from '../../actions/rdv'

export class Rdv extends Component {

    static propTypes = {
        rdvs : PropTypes.array.isRequired,
        getRDVs : PropTypes.func.isRequired,
        deleteRDV : PropTypes.func.isRequired
    }

    componentDidMount() {
        this.props.getRDVs();
    }

    render() {
        return (
            <Fragment>
                <h2>RDVs</h2>
                <table>
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Date</th>
                            <th>Lieu</th>
                            <th>Statut</th>
                            <th/>
                        </tr>
                    </thead>
                    <tbody>
                        { this.props.rdvs.map(rdv => (
                            <tr key= {rdv.id}>
                                <td>{rdv.id}</td>
                                <td>{rdv.date}</td>
                                <td>{rdv.lieu}</td>
                                <td>{rdv.statut}</td>
                                <td><button onClick={this.props.deleteRDV.bind(this, rdv.id)}>Delete</button></td>
                            </tr> 
                        ) ) }
                    </tbody>
                </table>
            </Fragment>
        )
    }
}

const mapStateToProps = state => ({
    rdvs: state.rdvReducer.rdvs
});

export default connect(
    mapStateToProps,
    { getRDVs, deleteRDV }
)(Rdv);
