import React, { Component } from 'react'
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { addUser } from '../../actions/users'


export class Form extends Component {

    state = {
        id : "",
        nom : "",
        prenom : "",
        datenaissance : "",
        mail : "",
        numerotel : "",
        password : ""
    }

    static propTypes = {
        addUser: PropTypes.func.isRequired
    }

    onChange = event => this.setState({[event.target.name] : event.target.value});
    onSubmit = event => {
        event.preventDefault();
        const { id, nom, prenom, datenaissance, mail, numerotel, password } = this.state;
        const user  = { id, nom, prenom, datenaissance, mail, numerotel, password };
        this.props.addUser(user);
        this.setState({
            id : "",
            nom : "",
            prenom : "",
            datenaissance : "",
            mail : "",
            numerotel : "",
            password : ""
        })
    }
    render() {
        const { id, nom, prenom, datenaissance, mail, numerotel, password } = this.state;
        return (
            <div>
                <h1>Add Users Form</h1>
                <form onSubmit={this.onSubmit}>
                    <div>
                        <label>Id</label>
                        <input
                            type="number"
                            name="id"
                            onChange={this.onChange}
                            value={id}
                        />
                    </div>
                    <div>
                        <label>Nom</label>
                        <input
                            type="text"
                            name="nom"
                            onChange={this.onChange}
                            value={nom}
                        />
                    </div>
                    <div>
                        <label>Prenom</label>
                        <input
                            type="text"
                            name="prenom"
                            onChange={this.onChange}
                            value={prenom}
                        />
                    </div>
                    <div>
                        <label>Date de Naissance</label>
                        <input
                            type="date"
                            name="datenaissance"
                            onChange={this.onChange}
                            value={datenaissance}
                        />
                    </div>
                    <div>
                        <label>E-mail</label>
                        <input
                            type="email"
                            name="mail"
                            onChange={this.onChange}
                            value={mail}
                        />
                    </div>
                    <div>
                        <label>Numero de Téléphone</label>
                        <input
                            type="tel"
                            name="numerotel"
                            onChange={this.onChange}
                            value={numerotel}
                        />
                    </div>
                    <div>
                        <label>Mot de passe</label>
                        <input
                            type="password"
                            name="password"
                            onChange={this.onChange}
                            value={password}
                        />
                    </div>
                    <div>
                        <button type="submit">
                            Envoyer
                        </button>
                    </div>
                </form>
            </div>
        )
    }
}

export default connect(null, { addUser })(Form);
