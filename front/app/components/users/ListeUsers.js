import React, { Fragment } from 'react';
import Form from './Form';
import Users from './Users'

export default function ListeUsers() {
    return (
        <Fragment>
            <Form/>
            <Users/>
        </Fragment>
    )
}
