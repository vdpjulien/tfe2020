import { combineReducers } from 'redux'; 
import userReducer from './userReducer'
import messageReducer from './messageReducer'
import rdvReducer from './rdvReducer'
import calendarReducer from './calendarReducer'

export default combineReducers({
    userReducer,
    rdvReducer,
    messageReducer,
    calendarReducer
});