import { GET_RDVS, DELETE_RDV, ADD_RDV } from '../actions/types'

const initialState = {
    rdvs: []
}

export default function(state = initialState, action){
    switch(action.type){
        case GET_RDVS:
            return {
                ...state,
                rdvs: action.payload
            };
        case DELETE_RDV:
            return {
                ...state,
                rdvs: state.rdvs.filter(rdv => rdv.id != action.payload)
            };
        case ADD_RDV:
            return {
                ...state,
                rdvs: [...state.rdvs, action.payload]
            };    
        default:
            return state;
    }
}
